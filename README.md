# DOC Praktek 1 Pertemuan 1-3

input
```
data("Kabupaten Aceh Barat", 0);
data("Kabupaten Aceh Besar", 0);
data("Kabupaten Batanghari", 1001);
data("Kabupaten Batanghari", 999);
data("Kabupaten Bungo", 0);
data("Kabupaten Lampung Timur", 0);
data("Kabupaten Lampung Utara", 0);
data("Kabupaten Bangli", 1001);
data("Kabupaten Buleleng", 1001);
data("Kabupaten Asmat", 0);
data("Kabupaten Biak Numfor", 0);
```

output
```
Buah pisang
1
2
3
4
5
6
7
8
9
10
Jumlah penduduk kabupaten batanghari > 1000
Jumlah penduduk kabupaten batanghari < 1000
1
2
3
4
Buah apel
Buah apel
Jumlah penduduk kabupaten batanghari > 1000
Jumlah penduduk kabupaten batanghari > 1000
angka 30 sd 40
angka 30 sd 40
```